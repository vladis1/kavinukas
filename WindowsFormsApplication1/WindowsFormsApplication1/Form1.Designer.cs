﻿namespace WindowsFormsApplication1
{
    partial class KavosAparatas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ekranas = new System.Windows.Forms.TextBox();
            this.juoda = new System.Windows.Forms.Button();
            this.balta = new System.Windows.Forms.Button();
            this.late = new System.Windows.Forms.Button();
            this.kava4 = new System.Windows.Forms.Button();
            this.kava5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ekranas
            // 
            this.ekranas.Location = new System.Drawing.Point(143, 23);
            this.ekranas.Multiline = true;
            this.ekranas.Name = "ekranas";
            this.ekranas.Size = new System.Drawing.Size(312, 270);
            this.ekranas.TabIndex = 0;
            this.ekranas.TextChanged += new System.EventHandler(this.ekranas_TextChanged);
            // 
            // juoda
            // 
            this.juoda.Location = new System.Drawing.Point(24, 67);
            this.juoda.Name = "juoda";
            this.juoda.Size = new System.Drawing.Size(75, 23);
            this.juoda.TabIndex = 1;
            this.juoda.Text = "juoda";
            this.juoda.UseVisualStyleBackColor = true;
            // 
            // balta
            // 
            this.balta.Location = new System.Drawing.Point(24, 97);
            this.balta.Name = "balta";
            this.balta.Size = new System.Drawing.Size(75, 23);
            this.balta.TabIndex = 2;
            this.balta.Text = "balta";
            this.balta.UseVisualStyleBackColor = true;
            // 
            // late
            // 
            this.late.Location = new System.Drawing.Point(24, 127);
            this.late.Name = "late";
            this.late.Size = new System.Drawing.Size(75, 23);
            this.late.TabIndex = 3;
            this.late.Text = "late";
            this.late.UseVisualStyleBackColor = true;
            // 
            // kava4
            // 
            this.kava4.Location = new System.Drawing.Point(24, 157);
            this.kava4.Name = "kava4";
            this.kava4.Size = new System.Drawing.Size(75, 23);
            this.kava4.TabIndex = 4;
            this.kava4.Text = "kava4";
            this.kava4.UseVisualStyleBackColor = true;
            // 
            // kava5
            // 
            this.kava5.Location = new System.Drawing.Point(24, 187);
            this.kava5.Name = "kava5";
            this.kava5.Size = new System.Drawing.Size(75, 23);
            this.kava5.TabIndex = 5;
            this.kava5.Text = "kava5";
            this.kava5.UseVisualStyleBackColor = true;
            // 
            // KavosAparatas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 319);
            this.Controls.Add(this.kava5);
            this.Controls.Add(this.kava4);
            this.Controls.Add(this.late);
            this.Controls.Add(this.balta);
            this.Controls.Add(this.juoda);
            this.Controls.Add(this.ekranas);
            this.Name = "KavosAparatas";
            this.Text = "KavosAparatas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ekranas;
        private System.Windows.Forms.Button juoda;
        private System.Windows.Forms.Button balta;
        private System.Windows.Forms.Button late;
        private System.Windows.Forms.Button kava4;
        private System.Windows.Forms.Button kava5;
    }
}

